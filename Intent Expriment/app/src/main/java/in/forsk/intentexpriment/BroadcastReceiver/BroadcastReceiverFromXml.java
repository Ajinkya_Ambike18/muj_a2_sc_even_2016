package in.forsk.intentexpriment.BroadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Saurabh on 3/2/2016.
 */

public class BroadcastReceiverFromXml extends BroadcastReceiver {
    private final static String TAG = BroadcastReceiverFromXml.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.e(TAG, "Action Received : "+action);

        Toast.makeText(context,"Broadcast Receiver Toast",Toast.LENGTH_LONG).show();

    }
}
